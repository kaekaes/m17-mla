﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoints : MonoBehaviour{

	public event EventHandler onPlayerPassCheckPoint;
	public event EventHandler onPlayerWrongsCheckPoint;
	public event EventHandler onPlayerEndsCheckPoints;

	public Color red;
	public Color green;
	public Color yellow;

	List<CheckPointOne> cps = new List<CheckPointOne>();
	int next;

	public void Awake() {
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild(i).TryGetComponent<CheckPointOne>(out CheckPointOne cp);
			cp.Set(this);
			cps.Add(cp);
		}
		Reset();
	}

	public void ThroughCheck(CheckPointOne cp) {
		if (cps.IndexOf(cp) == next) {
			//nice one boooi

			if (next + 1 >= cps.Count) {
				//te pasaste de wea
				onPlayerEndsCheckPoints?.Invoke(this, EventArgs.Empty);
			} else {
				next++;
				cp.transform.GetComponent<SpriteRenderer>().color = green;
                cp.transform.GetComponent<BoxCollider2D>().enabled = false;
				onPlayerPassCheckPoint?.Invoke(this, EventArgs.Empty);
			}
		} else {
			//wrong puta
			onPlayerWrongsCheckPoint?.Invoke(this, EventArgs.Empty);
		}

	}

	public void Reset() {
		next = 0;
		foreach (CheckPointOne cp in cps) {
			cp.transform.GetComponent<SpriteRenderer>().color = yellow;
            cp.transform.GetComponent<BoxCollider2D>().enabled = true;
        }
    }

	public CheckPointOne GetNext() {
		CheckPointOne nextcp = cps[next];
		nextcp.transform.GetComponent<SpriteRenderer>().color = red;
		return nextcp;
	}
}
